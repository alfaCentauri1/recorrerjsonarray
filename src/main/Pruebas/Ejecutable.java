package Pruebas;

import org.json.JSONArray;
import org.json.JSONObject;

public class Ejecutable{
    public static void main(String[] args) {
        String dataTesting = "{\n" +
                "    \"Informacion\": {\n" +
                "        \"evento\": \"TEST\",\n" +
                "        \"nombre\": \"Factura\",\n" +
                "        \"fechaHoraGeneracion\": \"2023-04-26T12:34:37.827Z\",\n" +
                "    },\n" +
                "    \"compras\": [\n" +
                "        {\n" +
                "            \"nombre\": \"Mancuernas\",\n" +
                "            \"subDetalle\": [\n" +
                "                {\n" +
                "                    \"articulo\": 1,\n" +
                "                    \"valor\": 1.5\n" +
                "                },\n" +
                "                {\n" +
                "                    \"articulo\": 2,\n" +
                "                    \"valor\": 2.6\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"nombre\": \"Barra de pesas 3/4 pulg.\",\n" +
                "            \"subDetalle\": [\n" +
                "                {\n" +
                "                    \"articulo\": 2,\n" +
                "                    \"valor\": 2.8\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        try {
            JSONObject jsonObject = new JSONObject(dataTesting);
            // Obtener el arreglo "compras"
            JSONArray comprasArray = jsonObject.getJSONArray("compras");
            System.out.println("Arreglo 'compras':");
            for (int i = 0; i < comprasArray.length(); i++) {
                JSONObject compraObject = comprasArray.getJSONObject(i);
                String nombreCompra = compraObject.getString("nombre");
                System.out.println("Nombre: " + nombreCompra);

                // Obtener el arreglo "subDetalle"
                JSONArray subDetalleArray = compraObject.getJSONArray("subDetalle");
                System.out.println("Subdetalle:");
                for (int j = 0; j < subDetalleArray.length(); j++) {
                    JSONObject subDetalleObject = subDetalleArray.getJSONObject(j);
                    int articulo = subDetalleObject.getInt("articulo");
                    double valor = subDetalleObject.getDouble("valor");
                    System.out.println("Artículo: " + articulo + ", Valor: " + valor);
                }
                System.out.println("----------------------");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}