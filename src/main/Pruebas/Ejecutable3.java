package Pruebas;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Ejecutable3 {

    public static void main(String[] args) {
        String dataTesting = "{\n" +
                "    \"Informacion\": {\n" +
                "        \"evento\": \"TEST\",\n" +
                "        \"nombre\": \"Factura\",\n" +
                "        \"fechaHoraGeneracion\": \"2023-04-26T12:34:37.827Z\",\n" +
                "    },\n" +
                "    \"compras\": [\n" +
                "        {\n" +
                "            \"nombre\": \"Mancuernas\",\n" +
                "            \"subDetalle\": [\n" +
                "                {\n" +
                "                    \"articulo\": 1,\n" +
                "                    \"valor\": 1.5\n" +
                "                },\n" +
                "                {\n" +
                "                    \"articulo\": 2,\n" +
                "                    \"valor\": 2.6\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"nombre\": \"Barra de pesas 3/4 pulg.\",\n" +
                "            \"subDetalle\": [\n" +
                "                {\n" +
                "                    \"articulo\": 2,\n" +
                "                    \"valor\": 2.8\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    ],\n" +
                "    \"extras\": [\n" +
                "        {\n" +
                "           \"direccion\": \"Avenida 1 con calle 14\",\n" +
                "           \"numero\": \"14-B\",\n" +
                "        }\n" +
                "    ],\n" +
                "}\n";

        try {
            JSONObject jsonObject = new JSONObject(dataTesting);

            // Recorrer el JSON de forma recursiva y obtener los arreglos
            List<JSONArray> arrays = new ArrayList<>(); //Debe ir vacio
            recorrerJSON(jsonObject, arrays);
            int i = 0;
            // Imprimir los arreglos encontrados
            for (JSONArray array : arrays) {
                System.out.println("Arreglo[" + i + "]: " + array.toString());
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param jsonObject
     * @param arrays
     **/
    protected static void recorrerJSON(JSONObject jsonObject, List<JSONArray> arrays) {
        for (String key : jsonObject.keySet()) {
            Object value = jsonObject.get(key);

            if (value instanceof JSONObject) {
                recorrerJSON((JSONObject) value, arrays);
            } else if (value instanceof JSONArray) {
                arrays.add((JSONArray) value);
            }
        }
    }
}
