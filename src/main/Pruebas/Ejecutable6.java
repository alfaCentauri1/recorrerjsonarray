package Pruebas;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Ejecutable6 {

    public static void main(String[] args) {
        String dataTesting = "{\n" +
                "    \"Informacion\": {\n" +
                "        \"evento\": \"TEST\",\n" +
                "        \"nombre\": \"Factura\",\n" +
                "        \"fechaHoraGeneracion\": \"2023-04-26T12:34:37.827Z\",\n" +
                "    },\n" +
                "    \"compras\": [\n" +
                "        {\n" +
                "            \"nombre\": \"Mancuernas\",\n" +
                "            \"subDetalle\": [\n" +
                "                {\n" +
                "                    \"articulo\": 1,\n" +
                "                    \"valor\": 1.5\n" +
                "                },\n" +
                "                {\n" +
                "                    \"articulo\": 2,\n" +
                "                    \"valor\": 2.6\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"nombre\": \"Barra de pesas 3/4 pulg.\",\n" +
                "            \"subDetalle\": [\n" +
                "                {\n" +
                "                    \"articulo\": 2,\n" +
                "                    \"valor\": 2.8\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    ],\n" +
                "    \"extras\": [\n" +
                "        {\n" +
                "           \"direccion\": \"Avenida 1 con calle 14\",\n" +
                "           \"numero\": \"14-B\",\n" +
                "        }\n" +
                "    ],\n" +
                "}\n";

        try {
            int i=0;
            JSONObject jsonObject = new JSONObject(dataTesting);

            // Recorrer el JSON de forma recursiva y obtener los arreglos
            Map<String, JSONArray> setArrays = new HashMap<>(); //Debe ir vacio
            generarCiclo(jsonObject, setArrays,i);

            int countSetArrays = setArrays.size();
            String[] tags = setArrays.keySet().toArray(new String[0]);
            printArrayList(countSetArrays, tags, setArrays);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** Imprimir los arreglos encontrados **/
    public static void printArrayList(int countSetArrays, String[] tags, Map<String, JSONArray> setArrays) {
        System.out.println("countSetArrays = " + countSetArrays + ", tags = " + Arrays.deepToString(tags) + ", setArrays = " + setArrays);
        for (int i=0; i<countSetArrays; i++) {
            JSONArray element = setArrays.get(tags[i]);
            System.out.println("Arreglo[" + i + "]: key: " + tags[i] + ": " + element);
        }
        System.out.println("Fin");
    }

    /**
     * Loop through a json.
     * @param jsonObject    Type JSONObject.
     * @param arrays        Type Map.
     **/
    protected static void loopThroughAJson(JSONObject jsonObject, Map<String, JSONArray> arrays) {
        for (String key : jsonObject.keySet()) {
            Object value = jsonObject.get(key);

            if (value instanceof JSONObject) {
                loopThroughAJson((JSONObject) value, arrays);
            } else if (value instanceof JSONArray) {
                arrays.put(key,(JSONArray) value);
            }
        }
    }

    /**
     *
     * @param jsonObject    Type JSONObject, this is json to search.
     * @param setArrays     Type Map<String, JSONArray>, set arrays.
     * @param k             Type int, number of call of the method.
     **/
    public static void generarCiclo(JSONObject jsonObject, Map<String, JSONArray> setArrays, int k) {
        int totalSetArrays = setArrays.size();
        if ( jsonObject != null && !jsonObject.isEmpty() ) {
            loopThroughAJson(jsonObject, setArrays);
            for ( String key : jsonObject.keySet() ) {
                JSONArray element = setArrays.get(key);
                if ( element != null ) {
                    System.out.println("Elemento del setArrays" + element);
                    Map<String, JSONArray> currentJsonArray = new HashMap<>();
                    currentJsonArray.put(key, element);
                    //Salida
                    System.out.println("Estamos en la llamada #" + k);
                    System.out.println("Nombre del arreglo: " + key);
                    System.out.println("Contenido de arreglo: " + element);
                    //Se debe entrar al contenido del arreglo
                    for (Object subElement : element) {
                        if (subElement instanceof JSONObject) {
                            System.out.println("Contenido del objeto: " + subElement);
                            generarCiclo((JSONObject) subElement, setArrays, k++);
                        }
                    }
                }
            }
        }
        else {
            System.out.println("No tiene sub-elementos");
        }
    }
}
