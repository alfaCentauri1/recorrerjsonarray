package Pruebas;

import org.json.JSONArray;
import org.json.JSONObject;

public class Ejecutable2 {

    public static void main(String[] args){
        String dataTesting = "{\n" +
                "    \"Informacion\": {\n" +
                "        \"evento\": \"TEST\",\n" +
                "        \"nombre\": \"Factura\",\n" +
                "        \"fechaHoraGeneracion\": \"2023-04-26T12:34:37.827Z\",\n" +
                "    },\n" +
                "    \"compras\": [\n" +
                "        {\n" +
                "            \"nombre\": \"Mancuernas\",\n" +
                "            \"subDetalle\": [\n" +
                "                {\n" +
                "                    \"articulo\": 1,\n" +
                "                    \"valor\": 1.5\n" +
                "                },\n" +
                "                {\n" +
                "                    \"articulo\": 2,\n" +
                "                    \"valor\": 2.6\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"nombre\": \"Barra de pesas 3/4 pulg.\",\n" +
                "            \"subDetalle\": [\n" +
                "                {\n" +
                "                    \"articulo\": 2,\n" +
                "                    \"valor\": 2.8\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        try {
            JSONObject jsonObject = new JSONObject(dataTesting);

            // Recorrer el JSON de forma recursiva
            recorrerJSON(jsonObject, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void recorrerJSON(JSONObject jsonObject, String indent) {
        for (String key : jsonObject.keySet()) {
            Object value = jsonObject.get(key);

            if (value instanceof JSONObject) {
                System.out.println(indent + "Objeto: " + key);
                recorrerJSON((JSONObject) value, indent + "\t");
            } else if (value instanceof JSONArray) {
                System.out.println(indent + "Arreglo: " + key);
                recorrerJSONArray((JSONArray) value, indent + "\t");
            } else {
                System.out.println(indent + key + ": " + value);
            }
        }
    }

    public static JSONArray recorrerJSONArray(JSONArray jsonArray, String indent) {
        JSONArray response = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            Object value = jsonArray.get(i);

            if (value instanceof JSONObject) {
                recorrerJSON((JSONObject) value, indent + "\t");
            } else if (value instanceof JSONArray) {
                recorrerJSONArray((JSONArray) value, indent + "\t");
            } else {
                System.out.println(indent + value);
                response.put(value);
            }
        }
        return response;
    }
}
